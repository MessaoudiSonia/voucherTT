/**
 * View Models used by Spring MVC REST controllers.
 */
package com.inetum.vouchertt.web.rest.vm;
